package com.timbrooke.cancerresearch {
	/**
	 * @author tim
	 */
	public class ChromosoneDatum {
		private var _chromoNum : int;
		private var _position : Number;
		private var _frequency : Number;

		public function ChromosoneDatum(cn:int,pos:Number,freq:Number) {
			_chromoNum = cn;
			_position = pos;
			_frequency = freq;
		}
		
		static public function fromLine(aLine:String):ChromosoneDatum{
			var parts:Array = aLine.split("\t");
			//trace(parts);
			if(parts.length==3){
				return new ChromosoneDatum(parseInt(parts[0]),parseInt(parts[1]),parseFloat(parts[2]));
			} else  {
				return null;
			}
		}


		public function get chromoNum() : int {
			return _chromoNum;
		}

		public function set chromoNum(chromoNum : int) : void {
			this._chromoNum = chromoNum;
		}

		public function get position() : Number {
			return _position;
		}

		public function set position(position : Number) : void {
			this._position = position;
		}

		public function get frequency() : Number {
			return _frequency;
		}

		public function set frequency(frequency : Number) : void {
			this._frequency = frequency;
		}
	}
}
