package com.timbrooke.cancerresearch {
	import flash.display.Sprite;

	/**
	 * @author tim
	 */
	public class Wrapper extends Sprite {
		public function Wrapper() {
			var m:Main = new Main();
			m.scaleX = .5;
			m.scaleY = .5;
			m.y = - 200;
			this.addChild(m);		
		}
	}
}
