package com.timbrooke.cancerresearch {
	import com.timbrooke.utils.messaging.IMessageListener;
	import com.timbrooke.utils.messaging.Message;
	import com.timbrooke.utils.messaging.MessageCenter;

	import flash.display.Graphics;
	import flash.display.Sprite;

	/**
	 * @author tim
	 */
	public class Main extends Sprite implements IMessageListener{
		private var mainModel:ChromosomeModel;
		private var view:ChromoView;
		
		public function Main() {
			
			MessageCenter.getInstance().addListener(this, CRMessages.DATA_READY);
			
			mainModel = new ChromosomeModel();
			mainModel.readFromFile("S3_BAF_Chrom1.txt");
		}

		public function handleMessage(message : Message) : void {
			
			switch(message.type){
				case CRMessages.DATA_READY:
					initGfx();
					break;
			}
		}

		private function initGfx() : void {
			var g:Graphics = this.graphics;
			g.beginFill(0x0);
			g.drawRect(0, 0, 640, 960);		
			view = new ChromoView(mainModel);
			this.addChild(view);
			view.update();	
		}
	}
}
