package com.timbrooke.cancerresearch {
	import com.timbrooke.utils.colours.Colours;

	import flash.display.BlendMode;
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;

	/**
	 * @author tim
	 */
	public class ChromoView extends Sprite {
		private var model : ChromosomeModel;
		private var g : Graphics;
		private var sg : Graphics;
		private var shape : Shape;
		private var sweep : Shape;

		public function ChromoView(aModel : ChromosomeModel) {
			model = aModel;
			shape = new Shape();
			g = shape.graphics;
			this.addChild(shape);
			shape.x = 320;
			shape.y = 480;
			this.addEventListener(Event.ENTER_FRAME, handleEnterFrame);

			createSweep();
		}

		private function createSweep() : void {
			var r : Number = 260;
			var angle : Number = 10;
			sweep = new Shape();
			sg = sweep.graphics;
			sg.beginFill(0x808080);
			sg.moveTo(0, 0);
			sg.lineTo(r, 0);
			for (var i : Number = 0;i <= angle;i += 0.5) {
				var x : Number = r * Math.cos(Math.PI * i / 180.0);
				var y : Number = r * Math.sin(Math.PI * i / 180.0);
				sg.lineTo(x, y);
			}
			sg.lineTo(0, 0);
			sg.endFill();
			sweep.x = 320;
			sweep.y = 480;
			sweep.blendMode = BlendMode.ADD;
			this.addChild(sweep);
		}

		private function handleEnterFrame(event : Event) : void {
			// count+=1;
			// update(count);
			sweep.rotation += 0.5;
		}

		public function update(offset : int = 0) : void {
			var l : int = 20000;
			// var offset:int = 1000;

			var raw : Point;
			var firstX : Number = model.getNormalisedValueAtIndex(offset).x;
			var lastX : Number = model.getNormalisedValueAtIndex(offset + l).x;
			var diffX : Number = lastX - firstX;
			var x : Number,y : Number;

			var angle : Number;
			var r : Number;

			g.clear();
			for (var i : int = 0;i < l;i++) {
				raw = model.getNormalisedValueAtIndex(i + offset);
				angle = ((raw.x - firstX) / diffX) * 2 * Math.PI;
				var radius : Number = 200;
				r = raw.y * radius + 60;
				x = Math.cos(angle) * r;
				y = Math.sin(angle) * r;

				var col : Object = tweenCol({r:27, g:126, b:224}, {r:224, g:86, b:27}, raw.y);
				var colHex : int = Colours.rgb2hex(col.r, col.g, col.b);
				drawSpot(x, y, colHex);
			}
		}

		public function drawSpot(x : Number, y : Number, col : int) : void {
			g.beginFill(col);
			g.drawRect(x, y, 2, 2);
			g.endFill();
		}

		public function tweenCol(a : Object, b : Object, t : Number) : Object {
			var c : Object = {};
			var parts : Array = ["r", "g", "b"];
			for each (var i:String  in parts) {
				c[i] = t * (b[i] - a[i]) + a[i];
			}

			return c;
		}
	}
}
