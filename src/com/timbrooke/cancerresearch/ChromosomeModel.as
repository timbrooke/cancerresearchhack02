package com.timbrooke.cancerresearch {
	import com.timbrooke.utils.messaging.Message;
	import com.timbrooke.utils.messaging.MessageCenter;

	import flash.events.Event;
	import flash.geom.Point;
	import flash.net.URLLoader;
	import flash.net.URLRequest;

	/**
	 * @author tim
	 */
	public class ChromosomeModel {
		private var chromo : Vector.<ChromosoneDatum>;
		private var chromoNorm : Vector.<Point>;
		private var ready : Boolean = false;
		private var _maxFreq : Number;
		private var _minFreq : Number;
		private var _maxPosition : Number;
		private var _minPosition : Number;
		private var _positionDiff : Number;
		private var _freqDiff : Number;

		public function ChromosomeModel() {
		}

		public function readFromFile(aFilename : String) : void {
			var loader : URLLoader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, onLoaded);
			loader.load(new URLRequest(aFilename));
		}

		function onLoaded(e : Event) : void {
			var myArrayOfLines : Array = e.target.data.split(/\n/);
			var l : int = myArrayOfLines.length;
			chromo = new Vector.<ChromosoneDatum>();

			_minFreq = Number.MAX_VALUE;
			_maxFreq = Number.MIN_VALUE;
			_minPosition = Number.MAX_VALUE;
			_maxPosition = Number.MIN_VALUE;
			var d : ChromosoneDatum;
			for (var i : int = 0;i < l;i++) {
				d = ChromosoneDatum.fromLine(myArrayOfLines[i]);
				if (d != null) {
					chromo.push(d);
					if (maxFreq < d.frequency) {
						//trace("MAX FREQ"+ maxFreq+" "+ d.frequency);
						_maxFreq = d.frequency;
						_freqDiff = maxFreq - minFreq;
					}
					if (minFreq > d.frequency) {
						//trace("MIN FREQ"+ minFreq+" "+ d.frequency);
						_minFreq = d.frequency;
						_freqDiff = maxFreq - minFreq;
					}
					if (minPosition > d.position) {
						trace(i+"->New min "+d.position);
						_minPosition = d.position;
						_positionDiff = maxPosition - minPosition;
					}
					if (maxPosition < d.position) {
						_maxPosition = d.position;
						//trace("New max pos"+_maxPosition);
						_positionDiff = maxPosition - minPosition;
					}
				}
			}
			chromoNorm = new Vector.<Point>();
			l = chromo.length;
			for (i = 0;i < l;i++) {
				d = chromo[i];
				var y : Number = (d.frequency - _minFreq) / _freqDiff;
				var x : Number = (d.position - _minPosition) / _positionDiff;
				chromoNorm.push(new Point(x, y));
			}
			ready = true;
			var message : Message = new Message(CRMessages.DATA_READY, this, {id:1});
			MessageCenter.getInstance().postMessage(message);
		}

		public function get maxFreq() : Number {
			return _maxFreq;
		}

		public function get minFreq() : Number {
			return _minFreq;
		}

		public function get maxPosition() : Number {
			return _maxPosition;
		}

		public function get minPosition() : Number {
			return _minPosition;
		}

		public function getValueAtIndex(i : int) : ChromosoneDatum {
			return chromo[i];
		}

		public function getNumDataPts() : int {
			return chromo.length;
		}

		public function getNormalisedValueAtIndex(i : int) : Point {
			return chromoNorm[i];
		}

		public function get positionDiff() : Number {
			return _positionDiff;
		}
	}
}
