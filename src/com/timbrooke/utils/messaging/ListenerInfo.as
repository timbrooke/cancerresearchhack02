package com.timbrooke.utils.messaging {
	/**
	 * @author tim
	 */
	public class ListenerInfo {
		private var _listener:IMessageListener;
		private var _handler:Function;
		private var _from : *;
		private var _type : String;

		public function ListenerInfo(listener:IMessageListener,type:String,handler:Function=null,from:*=null) {
			_listener= listener;
			_handler = handler;
			_from = from;
			_type = type;
		}

		public function get listener() : IMessageListener {
			return _listener;
		}

		public function get handler() : Function {
			return _handler;
		}

		public function get from() : * {
			return _from;
		}

		public function get type() : String {
			return _type;
		}
	}
}
