package com.timbrooke.utils.messaging {
	import flash.utils.Dictionary;

	/**
	 * @author tim
	 */
	public class MessageCenter {
		private var listeners : Dictionary;
		private var messageTypes : Vector.<String>;
		private static var _instance : MessageCenter = null;

		public static function getInstance() : MessageCenter {
			if (_instance == null) {
				_instance = new MessageCenter();
			}
			return _instance;
		}

		public function MessageCenter() {
			listeners = new Dictionary();
			messageTypes = new Vector.<String>();
		}

		public function addListener(listener : IMessageListener, type : String, handler : Function = null, from : *=null) : void {
			var info : ListenerInfo = new ListenerInfo(listener, type, handler, from);
			var list : Vector.<ListenerInfo> = listeners[type];
			if (list == null) {
				list = new Vector.<ListenerInfo>();
				listeners[type] = list;
			}
			list.push(info);
		}
		
		public function postMessage(message:Message):void{
			var list : Vector.<ListenerInfo> = listeners[message.type];
			for each(var info:ListenerInfo in list){
				if(info.from == null || info.from === message.origin){
					if(info.handler != null){
						info.handler(message);			
					} else {
						info.listener.handleMessage(message);
					}
				}
			}
		}
	}
}
