package com.timbrooke.utils.messaging {
	/**
	 * @author tim
	 */
	public interface IMessageListener {
		function handleMessage(message:Message):void;
	}
}
