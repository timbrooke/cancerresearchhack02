package com.timbrooke.utils.messaging {
	/**
	 * @author tim
	 */
	public class Message {
		private var _type : String;
		private var _content : *;
		private var _origin: *;

		public function Message(type:String,origin:*,content:* = null) {
			_type = type;
			_content = content;
			_origin = origin;
		}

		public function get type() : String {
			return _type;
		}

		public function get content() : * {
			return _content;
		}

		public function get origin() : * {
			return _origin;
		}  
	}
}
