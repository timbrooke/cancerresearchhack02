package com.timbrooke.utils.colours { 
 /**
  * Colour handling routines
  * Culled from many places:
  *  - http://www.actionscript.org/forums/s...d.php3?t=15155
  * 
  * @version  1.0
  */
 public class Colours extends Object {
 
  /** 
   * Convert RGB values to hexadecimal number
   * @param Number r  Red value
   * @param Number g  Green value
   * @param Number b  Blue value
   * @return Number   RGB as a hex value
   * @access public
   */
  static public function rgb2hex ( r:Number, g:Number, b:Number ):Number {
   return ( r << 16 | g << 8 | b );
  }
 
  /**
   * Convert a hex number into rgb values
   * @param Number hex_col Hex colour to convert
   * @return Object   Object with r, g and b properties
   * @access public
   */
  static public function hex2rgb ( hex_col:Number ):Object {
       var red:Number   = hex_col >> 16;
       var green_blue:Number = hex_col - ( red << 16 );
       var green:Number  = green_blue >> 8 ;
       var blue:Number   = green_blue - ( green << 8 );
      return ({r:red, g:green, b:blue});
  }
 
  /**
   * Convert a hex colour to HSV colour
   * @param Number hex_col Hex colour to convert
   * @return Object   Object with h, s and v properties
   * @return Object   Object with h, s and v properties
   * @access public
   */
  static public function hex2hsv ( hex_col:Number ):Object {
    var rgb:Object = Colours.hex2rgb( hex_col );
   return Colours.rgb2hsv( rgb.r, rgb.g, rgb.b );
  }
 
  /**
   * Convert a set of HSV values to a hex colour
   * @param Number h  Hue value
   * @param Number s  Sautration value
   * @param Number v  'Value' value
   * @return Number   Hex colour to convert
   * @access public
   */
  static public function hsv2hex ( hue:Number, sat:Number, val:Number ):Number {
    var rgb:Object = Colours.hsv2rgb( hue, sat, val );
   return Colours.rgb2hex( rgb.r, rgb.g, rgb.b );
  }
 
  /**
   * Convert HSB (also known as HSV values) to a hex values
   * @param Number h  Hue value
   * @param Number s  Sautration value
   * @param Number v  'Value' value
   * @return Object   Object with r, g and b properties
   * @access public
   */
  static public function hsv2rgb ( hue:Number, sat:Number, val:Number ):Object {
       var red, grn, blu, i, f, p, q, t;
      hue%=360;
      if(val==0) {return({r:0, g:0, b:0});}
      sat/=100;
      val/=100;
      hue/=60;
      i = Math.floor(hue);
      f = hue-i;
      p = val*(1-sat);
      q = val*(1-(sat*f));
      t = val*(1-(sat*(1-f)));
      if (i==0) {red=val; grn=t; blu=p;}
      else if (i==1) {red=q; grn=val; blu=p;}
      else if (i==2) {red=p; grn=val; blu=t;}
      else if (i==3) {red=p; grn=q; blu=val;}
      else if (i==4) {red=t; grn=p; blu=val;}
      else if (i==5) {red=val; grn=p; blu=q;}
      red = Math.floor(red*255);
      grn = Math.floor(grn*255);
      blu = Math.floor(blu*255);
      return ({r:red, g:grn, b:blu});
  }
 
  /** 
   * Convert RGB values to HSV value
   * @param Number r  Red value
   * @param Number g  Green value
   * @param Number b  Blue value
   * @return Object   Object with h, s and v properties
   * @access public
   */
  static public function rgb2hsv ( red:Number, grn:Number, blu:Number ):Object {
       var x, val, f, i, hue, sat;
      red/=255;
      grn/=255;
      blu/=255;
      x = Math.min(Math.min(red, grn), blu);
      val = Math.max(Math.max(red, grn), blu);
      if (x==val){ return({h:0, s:0, v:val*100});}
      f = (red == x) ? grn-blu : ((grn == x) ? blu-red : red-grn);
      i = (red == x) ? 3 : ((grn == x) ? 5 : 1);
      hue = Math.floor((i-f/(val-x))*60)%360;
      sat = Math.floor(((val-x)/val)*100);
      val = Math.floor(val*100);
      return({h:hue, s:sat, v:val});
  }
 }
}