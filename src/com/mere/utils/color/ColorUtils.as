package com.mere.utils.color
{
	public class ColorUtils
	{
		
		public static function hex2rgb(hex:uint):Array
		{
			var r:uint = hex >> 16 & 0xFF;
			var g:uint = hex >> 8 & 0xFF;
			var b:uint = hex & 0xFF;
			return [r,g,b];
		}
		
		public static function hex2String(hex:uint):String
		{
			var placeholder:String = '000000';
			var str:String = hex.toString(16);
			str = placeholder.substr(0,6-str.length)+str;
			return '#'+str;
		}
		
		public static function rgb2hex32(r:uint, g:uint, b:uint,alpha:uint):uint
		{
			return (alpha<<24) + (r<<16) + (g<<8) +b;
		}
		
		public static function rgb2hex(r:uint, g:uint, b:uint):uint
		{
			return (r<<16) + (g<<8) +b;
		}
		
		public static function hex2hsv(hex:uint):Array
		{
			var rgb:Array = hex2rgb(hex);
			return rgb2hsv(rgb[0], rgb[1], rgb[2] );
		}
			
		public static function hex2hsl(hex:uint):Array
		{
			var rgb:Array = hex2rgb(hex);
			return rgb2hsl(rgb[0], rgb[1], rgb[2] );
		}
			
		public static function rgb2hsv(r:uint, g:uint, b:uint):Array
		{
			var max:uint = Math.max(r, g, b);
			var min:uint = Math.min(r, g, b);
			
			var h:Number = 0;
			var s:Number = 0;
			var v:Number = 0;
			
			//get h
			if(max == min){
				h = 0;
			}else if(max == r){
				h = (60 * (g-b) / (max-min) + 360) % 360;
			}else if(max == g){
				h = (60 * (b-r) / (max-min) + 120);
			}else if(max == b){
				h = (60 * (r-g) / (max-min) + 240);
			}
			
			//get v
			v = max/255;
			
			//get s
			if(max == 0){
				s = 0;
			}else{
				s = (max - min) / max;
			}
			
			return [h, s, v];
		}
		
		public static function rgb2hsl(r:uint, g:uint, b:uint):Array
		{
			r /= 255;
			g /= 255;
			b /= 255;
			var max:Number = Math.max(r, g, b);
			var min:Number = Math.min(r, g, b);
			var h:Number;
			var s:Number;
			var l:Number= (max + min) / 2;
			
			if(max == min){
				h = s = 0; // achromatic
			}else{
				var d:Number = max - min;
				s = (l > 0.5 )? d / (2 - max - min) : d / (max + min);
				switch(max)
				{
					case r: h = (g - b) / d + (g < b ? 6 : 0); break;
					case g: h = (b - r) / d + 2; break;
					case b: h = (r - g) / d + 4; break;
				}
				h /= 6;
			}
			
			return [h, s, l];
		}
		
	}
}